# cli-commands
A selection of useful cli commands

## Maven

- `mvn clean -DskipTests=true release:prepare release:perform`
- `mvn -Dmaven.surefire.debug test` [Maven Surefire: Debugging Tests](https://maven.apache.org/surefire/maven-surefire-plugin/examples/debugging.html)
- `mvn -Dtest=TestClass#testFunction test`
- `mvn spring-boot:run -Dspring.profiles.active=sandbox` Start Spring Boot with a specific profile

## Kube Control

- `kubectl logs --namespace default svc/service-name-here -f`
- `kubectl port-forward --namespace default svc/service-name-here-db-prod 5432:5432`
- `kubectl describe pod pod-name-here-6f97c7879c-trlbn`
- `kubectl config use-context CONTEXT_NAME`
- `kubectl config get-contexts`

## Helm

```
helm upgrade service-name-here-prod deployment/service-name-here
    --install
    --force
    --wait
    --set fullnameOverride=service-name-here-prod
    -f deployment/service-name-here-config/prod/values.yaml
```

```
helm template service-name-here-prod service-name-here
    --values ./deployment/service-name-here-config/prod/values.yaml
    --show-only templates/my-configmap.yaml
```

## Ansible

- `ansible-playbook play.yaml -i environments/prod --tags "createAPIs"`
- `ansible-vault decrypt --vault-password-file=vault_password.txt my-secrets.vault.yaml`

## Docker

- `docker stats`
- `docker container run -it -p 8080:8080 my-docker-image:latest`
- `docker run -it --env SPRING_PROFILES_ACTIVE=test -p 8080:8080 -d my-service-image:latest`
- `docker run -it --entrypoint /bin/bash my-docker-image-to-debug:latest`
- `docker build -t my-registry.olof.info/docker/non-root-user-node:14 .`
- `docker push my-registry.olof.info/docker/non-root-user-node:14`
- `sudo usermod -aG docker $USER` (allow current user to use docker without sudo)
- `docker save -o my-docker-image.tar my-registry.com/my-docker-image`
- `docker load -i my-docker-image.tar`
```
docker build -f Dockerfile
    --tag service-name-here:latest
    --build-arg APP_VERSION="1.0.0"
    .
```

## Docker Compose

- `docker-compose logs -f -t --tail=30 my-service-name`

## Git

- `git log -p /myfile` Show git history for a single file

## GitLab

- `gitlab-rake "gitlab:password:reset"` (reset user password)
- `gitlab-rails console > User.where(username: "olof").each(&:disable_two_factor!)` (reset user 2fa)

## Bash

```sh
#!/bin/bash
set -eu -o pipefail
```

## SSH

Copy local public key to remote server (adds an entry in ~/.ssh/authorized_keys)
- `ssh-copy-id ubuntu@192.168.0.60`

Open a SSH connection and specify a start folder
- `ssh -t ubuntu "cd /srv/filer/docker/; bash"`

## nginx

- `nginx -t` test config
- `nginx -s reload` restart

## golang

- `go build -v -ldflags="-X 'github.com/nanu-c/axolotl/app/config.AppVersion=1.0.0'"`

## base64

- `echo -n ClientId:ClientSecret | base64`
- `echo "theBase64String" | base64 --decode`

## md5sum

- `echo -n myString | md5sum`

## oh-my-zsh

- `gcd -f && gl` (force-checkout develop and pull)
- `gpsup` (git push and create branch)
- `gcb` (git checkout and create branch)

## yq

- `yq read --doc '*' --colors -`

## openssl

- `openssl pkcs12 -in my-file.pfx -info`
- `openssl pkcs12 -in my-cert-archive-file.pfx -out cleartext-ssl-cert.out` Extract a certificate from a PFX archive
- `openssl rsa -in cleartext-ssl-cert.out -aes256 -out extracted-cert.pem` Encrypt the private key with RSA

## psql

- `PGPASSWORD=password psql --host=127.0.0.1 --username=postgres --dbname=postgres`

## npm

The location where global packages are installed (installed with -g)
- `npm root -g`

## mysql

- `MYSQL_PWD=password mysql --host=127.0.0.1 --user=mysql --database=mysql`
- `docker run -it --rm --env MYSQL_PWD=password mysql mysql --host=127.0.0.1 --user=mysql --database=mysql`

delete duplicated rows based on created at timestamp
```sql
DELETE r2 FROM report AS r1, report AS r2

WHERE r1.external_id = r2.external_id AND r1.created_at > r2.created_at
```

## bluetoothctl

- `power on`
- `scan on`
- `rm -rf /var/lib/bluetooth/<adapter mac>/<device mac>/` (to remove device)

## netplan

- `sudo vim /etc/netplan/00-installer-config.yaml` (ensure interface is correctly named)
- `sudo netplan generate`
- `sudo netplan apply`

## Network

- `curl icanhazip.com`
- `sudo arp-scan -l`
- `nmap -p 443 localhost` (check if local port is open)
- `nmcli dev wifi` (show wifi details)

## Arch Linux

- `sudo pacman -Rcns my-package`
- `yay -Sua` (this is the default, when just using `yay`)
- `pactree -r` (search for reverse dependencies)
- `sudo reflector -c de -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist` (update pacman mirrors)
- `paru -G vim` (download PKGBUILD)
- `pacman -Sy archlinux-keyring && pacman-key --populate archlinux && pacman-key --refresh-keys` (update pacman keys)

Repository icon made by [Kirill Kazachek](https://www.flaticon.com/authors/kirill-kazachek).
